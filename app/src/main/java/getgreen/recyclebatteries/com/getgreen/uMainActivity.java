package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by taras on 03.09.14.
 */
public class uMainActivity extends Activity {
    static final String PUT_TOKEN = "put_token";
    private Button uGoButton1;
    private Button uGoButton2;
    private TextView uYourBalance;
    String url;
    String balance;
    String uToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_main_activity);
        url = "http://api.check-host.ru/battery/user/getBalance?token=";
        uYourBalance = (TextView)findViewById(R.id.your_balance);
//        Intent intent = getIntent();
//        String token = intent.getStringExtra(PUT_TOKEN);
        Handler handler = new Handler();
        final Runnable r = new Runnable()
        {
            public void run()
            {
                readFile();
                final String finalUrl = url+uToken;
                Log.w("finalUrl",finalUrl);
                new DownloadFilesTask().execute(finalUrl);
            }
        };
        handler.postDelayed(r, 2000);
        uGoButton1 = (Button)findViewById(R.id.u_go_button1);
        uGoButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(uMainActivity.this, uActivityGeoposition.class);
                startActivity(intent);
            }
        });
        uGoButton2 = (Button)findViewById(R.id.u_go_button2);
        uGoButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(uMainActivity.this, uSpendGreens1.class);
                startActivity(intent);
            }
        });
    }

    public  class DownloadFilesTask extends AsyncTask<String, Integer, Void> {

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Void result) {
            uYourBalance.setText(balance+" грин");
        }

        @Override
        protected Void doInBackground(String... params) {
            String url = params[0];

// getting JSON string from URL
            JSONObject json = getJSONFromUrl(url);

//parsing json data
            parseJson(json);
            return null;
        }
    }
    public JSONObject getJSONFromUrl(String url) {
        InputStream is = null;
        JSONObject jObj = null;
        String json = null;

// Making HTTP request
        try {
// defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jObj = new JSONObject(json);
            Log.w("json", "" + jObj);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

// return JSON String
        return jObj;

    }
    public String parseJson(JSONObject json) {
        try {
            JSONItem data = new JSONItem();
            data.setResult(json.getString("result"));
            String result = data.getResult();
            Log.w("result",""+result);
            if(json.toString().contains("balance")) {
                data.setBalance(json.getString("balance"));
                balance = data.getBalance();
                Log.w("Balance", balance);
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return balance;
    }

    void readFile() {
        try {
            // открываем поток для чтения
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    openFileInput("file")));
            String str = "";
            // читаем содержимое
            while ((str = br.readLine()) != null) {
                Log.w("", str);
                uToken = str;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

