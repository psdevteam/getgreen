package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by taras on 19.09.14.
 */
public class uSpendGreens4 extends Activity {
    private TextView uYouHaveIssued;
    private TextView uQuantities;
    private TextView uPoint;
    private Button uEndButton;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_spend_greens4);

        uYouHaveIssued = (TextView)findViewById(R.id.u_you_have_issued);
        uQuantities = (TextView)findViewById(R.id.u_quantities);
        uPoint = (TextView)findViewById(R.id.u_point);
        uEndButton = (Button)findViewById(R.id.u_end_button);
        uEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(uSpendGreens4.this, uMainActivity.class);
//                startActivity(intent);
            }
        });
    }
}
