package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;


public class MainActivity extends Activity {
    static final String PUT_TOKEN = "put_token";
    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_TOKEN = "token";
    private Button mContinueButton1;
    private Button mContinueButton2;
    private Button mPartnerButton;
    private EditText mEnterToPhoneNumber;
    private EditText mEnterToCode;
    String FILENAME ="Token";
    String writeFile;
    String mAuthorization;
    String mNum;
    String mNumber;
    String url;
    String result;
    String token;
    String code;
    String mCode;
    String rightNum;
    int x = 0;
    SharedPreferences mSettings;
    TextView EnterYourPhoneNumber;
    TextView EnterTheCodeWeSentToYourPhone;
    Handler handler;


    ProgressDialog pd;

    // переменная, отображающая статус интернет соединения
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    @Override
    protected void onRestart(){
        super.onRestart();
        mEnterToCode.setText("");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        mEnterToPhoneNumber = (EditText)findViewById(R.id.m_enter_to_phone_number);
        mAuthorization = "http://api.check-host.ru/battery/user/login";
        mNum = "?num=7";
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        // creating connection detector class instance
        cd = new ConnectionDetector(getApplicationContext());

        mContinueButton1 = (Button)findViewById(R.id.m_continue_button1);
        mContinueButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EnterYourPhoneNumber = (TextView)findViewById(R.id.enter_your_phone_number);

                mNumber = mEnterToPhoneNumber.getText().toString();
                if ((mNumber.length() == 11)) {
                    rightNum = mNumber.substring(mNumber.length() - 10);
//                    Log.w("rightNum", "" + rightNum);
                    url = mAuthorization+mNum+rightNum;
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new DownloadFilesTask().execute(url);
//                    Log.w("красивый запрос",url);
                        EnterYourPhoneNumber.setVisibility(View.GONE);
                        mEnterToPhoneNumber.setVisibility(View.GONE);
                        mContinueButton1.setVisibility(View.GONE);

                        EnterTheCodeWeSentToYourPhone = (TextView) findViewById(R.id.enter_the_code_we_sent_to_your_phone);

                        EnterTheCodeWeSentToYourPhone.setVisibility(View.VISIBLE);
                        mEnterToCode.setVisibility(View.VISIBLE);
                        mContinueButton2.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(MainActivity.this, "Отсутсвует подключение к интернету", Toast.LENGTH_SHORT).show();
                    }
                } else
                {
                    Toast.makeText(MainActivity.this, "Вы неправильно набрали номер", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mEnterToCode = (EditText)findViewById(R.id.m_enter_to_code);

        mContinueButton2 = (Button)findViewById(R.id.m_continue_button2);
        mContinueButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCode = mEnterToCode.getText().toString();
                code = "&code=";
                if ((mCode.length() == 5)) {
                    url = mAuthorization+mNum+rightNum+code+mCode;
                    Log.w("codeurl",url);
                    new DownloadFilesTask().execute(url);
                    if (result.equalsIgnoreCase("200")) {
                        Log.w("onClickResult",result);

                        handler=new Handler();
                        final Runnable r = new Runnable()
                        {
                            public void run()
                            {
                                Intent intent = new Intent(MainActivity.this, uMainActivity.class);
//                                intent.putExtra(PUT_TOKEN, token);
//                                Log.w("intentToken", token);
                                if (x==0) {
                                writeFile(token); }
                                x++;
                                startActivity(intent);
                            }
                        };
                        handler.postDelayed(r, 1000);
                        EnterYourPhoneNumber.setVisibility(View.VISIBLE);
                        mEnterToPhoneNumber.setVisibility(View.VISIBLE);
                        mContinueButton1.setVisibility(View.VISIBLE);
                        EnterTheCodeWeSentToYourPhone.setVisibility(View.GONE);
                        mEnterToCode.setVisibility(View.GONE);
                        mContinueButton2.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(MainActivity.this, "Вы неправильно набрали код", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Код должен состоять из пяти цифр", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mPartnerButton = (Button)findViewById(R.id.m_partner_button);
        mPartnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, pMainActivity.class);
                startActivity(intent);
            }
        });
    }
    public  class DownloadFilesTask extends AsyncTask<String, Integer, Void> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Загрузка");
            pd.setTitle("Подождите");
            pd.show();

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Void result) {
         pd.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {
            String url = params[0];


// getting JSON string from URL
            JSONObject json = getJSONFromUrl(url);

//parsing json data
            parseJson(json);
            return null;
        }
    }
    public JSONObject getJSONFromUrl(String url) {
        InputStream is = null;
        JSONObject jObj = null;
        String json = null;

// Making HTTP request
        try {
// defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jObj = new JSONObject(json);
            Log.w("json", "" + jObj);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

// return JSON String
        return jObj;

    }
    public String parseJson(JSONObject json) {
        try {
            JSONItem data = new JSONItem();
            data.setResult(json.getString("result"));
            result = data.getResult();
            Log.w("result",""+result);
            if ((json.toString().contains("token"))){
            data.setToken(json.getString("token"));
            token = data.getToken();
       //      writeFile(token);
            Log.w("token",token);
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return token;
    }
    void writeFile(String mWriteToken) {
        try {
            // отрываем поток для записи
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                    openFileOutput(mWriteToken, MODE_PRIVATE)));
            // пишем данные
            bw.write("Содержимое файла");
            // закрываем поток
            bw.close();
            Log.w("Файл записан", "Файл записан");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
