package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by taras on 03.09.14.
 */
public class uSpendGreens2 extends Activity {
    private EditText uNumberOf;
    private Button uContinueButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_spend_greens2);

        uNumberOf = (EditText)findViewById(R.id.u_number_of);

        uContinueButton = (Button)findViewById(R.id.u_continue_button);
        uContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}