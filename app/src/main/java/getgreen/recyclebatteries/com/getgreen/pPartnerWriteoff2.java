package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by taras on 04.09.14.
 */
public class pPartnerWriteoff2 extends Activity {
    private EditText pNumberOfWrite;
    private Button pContinueButtonWrite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_partner_writeoff2);

        pNumberOfWrite = (EditText)findViewById(R.id.p_number_of_write);

        pContinueButtonWrite = (Button)findViewById(R.id.p_continue_button_write);
        pContinueButtonWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }
}