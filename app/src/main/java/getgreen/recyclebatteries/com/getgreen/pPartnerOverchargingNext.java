package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by taras on 20.09.14.
 */
public class pPartnerOverchargingNext extends Activity {
    private TextView pParticipantWithNumber;
    private TextView pNumberGreens;
    private Button pEndButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_partner_overcharging_next);
        pParticipantWithNumber = (TextView)findViewById(R.id.p_participant_with_number);
        pNumberGreens = (TextView)findViewById(R.id.p_number_greens);

        pEndButton = (Button)findViewById(R.id.p_end_button);
        pEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(pPartnerOverchargingNext.this, pMainActivity.class);
//                startActivity(intent);
            }
        });
    }
}
