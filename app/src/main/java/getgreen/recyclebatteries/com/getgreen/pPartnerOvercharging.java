package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by taras on 03.09.14.
 */
public class pPartnerOvercharging extends Activity {
    private EditText pNumberOf;
    private EditText pEnterCode;
    private Button pContinueButton;
    private String result;
    private String pNumber;

    String url;
    String pAuthorization;
    String pToken;
    String user_num;
    String code;
    String pCode;
    String amount;
    String pAmountGreens;

    // переменная, отображающая статус интернет соединения
    Boolean isInternetPresent = false;
    // Connection detector class
    ConnectionDetector cd;
    static final String EXTRA_MESSAGE = "pNumber";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_partner_overcharging);
        final Intent intent = getIntent();
        pNumber = intent.getStringExtra(EXTRA_MESSAGE);
        pAuthorization = "http://api.check-host.ru/battery/user/charge?";
        pToken = "token=6e320e30-2750-11e4-9ba3-b5b879359f7c";
        user_num = "&user_num=7";
        code = "&code=";
        amount = "&amount=";
        cd = new ConnectionDetector(getApplicationContext());


        pNumberOf = (EditText)findViewById(R.id.p_number_of);
        pEnterCode = (EditText)findViewById(R.id.p_enter_code);


        pContinueButton = (Button)findViewById(R.id.p_continue_button);
        pContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pAmountGreens = pNumberOf.getText().toString();
                pCode = pEnterCode.getText().toString();
                if ((pCode.length() == 5)) {
                    url = pAuthorization+pToken+user_num+pNumber+code+pCode+amount+pAmountGreens;
//                    Log.w("codeurl",url);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                    new DownloadFilesTask().execute(url);
                    Log.w("красивый запрос",url);
//                        Intent intent = new Intent(pPartnerOvercharging.this, pPartnerOverchargingNext.class);
                        Toast.makeText(pPartnerOvercharging.this, "Success!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(pPartnerOvercharging.this, "Отсутсвует подключение к интернету", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(pPartnerOvercharging.this, "Вы неправильно набрали код", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public  class DownloadFilesTask extends AsyncTask<String, Integer, Void> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Void result) {
//         pd.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {
            String url = params[0];

// getting JSON string from URL
            JSONObject json = getJSONFromUrl(url);

//parsing json data
            parseJson(json);
            return null;
        }
    }
    public JSONObject getJSONFromUrl(String url) {
        InputStream is = null;
        JSONObject jObj = null;
        String json = null;

// Making HTTP request
        try {
// defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jObj = new JSONObject(json);
            Log.w("json", "" + jObj);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

// return JSON String
        return jObj;

    }
    public void parseJson(JSONObject json) {
        try {
            JSONItem data = new JSONItem();
            data.setResult(json.getString("result"));
            result = data.getResult();
            Log.w("result",""+result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}