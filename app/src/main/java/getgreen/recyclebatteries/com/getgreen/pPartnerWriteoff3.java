package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by taras on 20.09.14.
 */
public class pPartnerWriteoff3 extends Activity {
    private EditText pEnterCodeToYou;
    private Button pContinueButton;
    String code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_partner_writeoff3);
        pEnterCodeToYou = (EditText)findViewById(R.id.p_enter_code_to_you);
        pContinueButton = (Button)findViewById(R.id.p_continue_button);
        pContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                code = pEnterCodeToYou.getText().toString();
                if (code.length()==5) {
                    Intent intent = new Intent(pPartnerWriteoff3.this, pPartnerWriteoff4.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(pPartnerWriteoff3.this, "Код должен состоять из пяти цифр", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
