package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by taras on 03.09.14.
 */
public class uActivityGeopositionNext extends Activity {
//    static final String PUT_POSE = "put_pose";
//    static final String PUT_POSE_LIST = "put_pose_list";
//    String pose;
//    String poseOnList;
    private TextView uTitle;
    private TextView uAddress;
    private TextView uOpeningHours;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_activity_geoposition_next);
//        Intent intent = getIntent();
//        pose = intent.getStringExtra(PUT_POSE);
//        poseOnList = intent.getStringExtra(PUT_POSE_LIST);
//        Log.w("PUT_POSE", pose);
//        Log.w("PUT_POSE_LIST", poseOnList);

        uTitle = (TextView)findViewById(R.id.title);
        uAddress = (TextView)findViewById(R.id.address);
        uOpeningHours = (TextView)findViewById(R.id.opening_hours);


        // массивы для отображения в ListView  по тапу на элементы Spinner
        String [] ChelyabinskArray = getResources().getStringArray(R.array.Chelyabinsk);
        final ArrayAdapter<String> adapterChelyabinsk = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ChelyabinskArray);
        String [] EkaterinburgArray = getResources().getStringArray(R.array.Ekaterinburg);
        final ArrayAdapter<String> adapterEkaterinburg = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, EkaterinburgArray);
        String [] IgevskArray = getResources().getStringArray(R.array.Igevsk);
        final ArrayAdapter<String> adapterIgevsk = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, IgevskArray);
        String [] KazanArray = getResources().getStringArray(R.array.Kazan);
        final ArrayAdapter<String> adapterKazan = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KazanArray);
        String [] KopeiskArray = getResources().getStringArray(R.array.Kopeisk);
        final ArrayAdapter<String> adapterKopeisk = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KopeiskArray);
        String [] KurganArray = getResources().getStringArray(R.array.Kurgan);
        final ArrayAdapter<String> adapterKurgan = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KurganArray);
        String [] KishtimArray = getResources().getStringArray(R.array.Kishtim);
        final ArrayAdapter<String> adapterKishtim = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KishtimArray);
        String [] MiassArray = getResources().getStringArray(R.array.Miass);
        final ArrayAdapter<String> adapterMiass = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, MiassArray);
        String [] RostovOnDonArray = getResources().getStringArray(R.array.RostovOnDon);
        final ArrayAdapter<String> adapterRostovOnDon = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, RostovOnDonArray);
        String [] SamaraArray = getResources().getStringArray(R.array.Samara);
        final ArrayAdapter<String> adapterSamara = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, SamaraArray);
        String [] SimArray = getResources().getStringArray(R.array.Sim);
        final ArrayAdapter<String> adapterSim = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, SimArray);
        String [] TimenArray = getResources().getStringArray(R.array.Timen);
        final ArrayAdapter<String> adapterTimen = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, TimenArray);
        String [] UfaArray = getResources().getStringArray(R.array.Ufa);
        final ArrayAdapter<String> adapterUfa = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, UfaArray);
        String [] HabarovskArray = getResources().getStringArray(R.array.Habarovsk);
        final ArrayAdapter<String> adapterHabarovsk = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, HabarovskArray);



        String [] ChelyabinskInfoArray = getResources().getStringArray(R.array.ChelyabinskInfo);
        final ArrayAdapter<String> adapterChelyabinskInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ChelyabinskInfoArray);
        String [] EkaterinburgInfoArray = getResources().getStringArray(R.array.EkaterinburgInfo);
        final ArrayAdapter<String> adapterEkaterinburgInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, EkaterinburgInfoArray);
        String [] IgevskInfoArray = getResources().getStringArray(R.array.IgevskInfo);
        final ArrayAdapter<String> adapterIgevskInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, IgevskInfoArray);
        String [] KazanInfoArray = getResources().getStringArray(R.array.KazanInfo);
        final ArrayAdapter<String> adapterKazanInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KazanInfoArray);
        String [] KopeiskInfoArray = getResources().getStringArray(R.array.KopeiskInfo);
        final ArrayAdapter<String> adapterKopeiskInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KopeiskInfoArray);
        String [] KurganInfoArray = getResources().getStringArray(R.array.KurganInfo);
        final ArrayAdapter<String> adapterKurganInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KurganInfoArray);
        String [] KishtimInfoArray = getResources().getStringArray(R.array.KishtimInfo);
        final ArrayAdapter<String> adapterKishtimInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KishtimInfoArray);
        String [] MiassInfoArray = getResources().getStringArray(R.array.MiassInfo);
        final ArrayAdapter<String> adapterMiassInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, MiassInfoArray);
        String [] RostovOnDonInfoArray = getResources().getStringArray(R.array.RostovOnDonInfo);
        final ArrayAdapter<String> adapterRostovOnDonInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, RostovOnDonInfoArray);
        String [] SamaraInfoArray = getResources().getStringArray(R.array.SamaraInfo);
        final ArrayAdapter<String> adapterSamaraInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, SamaraInfoArray);
        String [] SimInfoArray = getResources().getStringArray(R.array.SimInfo);
        final ArrayAdapter<String> adapterSimInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, SimInfoArray);
        String [] TimenInfoArray = getResources().getStringArray(R.array.TimenInfo);
        final ArrayAdapter<String> adapterTimenInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, TimenInfoArray);
        String [] UfaInfoArray = getResources().getStringArray(R.array.UfaInfo);
        final ArrayAdapter<String> adapterUfaInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, UfaInfoArray);
        String [] HabarovskInfoArray = getResources().getStringArray(R.array.HabarovskInfo);
        final ArrayAdapter<String> adapterHabarovskInfo = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, HabarovskInfoArray);

    }
}