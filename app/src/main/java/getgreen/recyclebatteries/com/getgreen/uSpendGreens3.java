package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by taras on 19.09.14.
 */
public class uSpendGreens3 extends Activity {
    private EditText uEnterCodeToYou;
    private Button uContinueButton;
    String code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_spend_greens3);

        uEnterCodeToYou = (EditText)findViewById(R.id.enter_code_to_you);
        uContinueButton = (Button)findViewById(R.id.u_continue_button);
        uContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                code = uEnterCodeToYou.getText().toString();
                if (code.length()==5) {
                    Intent intent = new Intent(uSpendGreens3.this, uSpendGreens4.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(uSpendGreens3.this, "Код должен состоять из пяти цифр", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
