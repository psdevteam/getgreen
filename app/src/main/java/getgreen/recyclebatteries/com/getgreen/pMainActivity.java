package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by taras on 03.09.14.
 */
public class pMainActivity extends Activity {
    private EditText pEditText;
    private Button pOverchargingGreensButton;
    private Button pWriteOffTheGreensButton;
    private String pNumber;
    private String rightNum;
    static final String EXTRA_MESSAGE = "pNumber";
    // переменная, отображающая статус интернет соединения
    Boolean isInternetPresent = false;
    // Connection detector class
    ConnectionDetector cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_main_activity);
        pEditText = (EditText)findViewById(R.id.p_edit_text);
        cd = new ConnectionDetector(getApplicationContext());


        pOverchargingGreensButton = (Button)findViewById(R.id.p_overcharging_greens_button);
        pOverchargingGreensButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rightNum = pEditText.getText().toString();
                if ((rightNum.length() == 11)) {
                    pNumber = rightNum.substring(rightNum.length() - 10);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                    Log.w("rightNumb", pNumber);
                    Intent intent = new Intent(pMainActivity.this, pPartnerOvercharging.class);
                    intent.putExtra(EXTRA_MESSAGE, pNumber);
                    startActivity(intent);
                    } else {
                        Toast.makeText(pMainActivity.this, "Отсутсвует подключение к интернету", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(pMainActivity.this, "Вы неправильно набрали номер", Toast.LENGTH_SHORT).show();
                }
            }
        });
        pWriteOffTheGreensButton = (Button)findViewById(R.id.p_write_off_the_greens_button);
        pWriteOffTheGreensButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(pMainActivity.this, pPartnerWriteoff1.class);
                startActivity(intent);
            }
        });
    }

}
