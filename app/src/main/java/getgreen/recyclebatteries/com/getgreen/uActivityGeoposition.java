package getgreen.recyclebatteries.com.getgreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

/**
 * Created by taras on 03.09.14.
 */
public class uActivityGeoposition extends Activity {
//    static final String PUT_POSE = "put_pose";
//    static final String PUT_POSE_LIST = "put_pose_list";
//    int pose;
//    int poseOnList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_activity_geoposition);
        final ListView uListViewGeoposition = (ListView)findViewById(R.id.u_list_view_geoposition);

// массивы для отображения в ListView  по тапу на элементы Spinner
        String [] ChelyabinskArray = getResources().getStringArray(R.array.Chelyabinsk);
        final ArrayAdapter<String> adapterChelyabinsk = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ChelyabinskArray);
        String [] EkaterinburgArray = getResources().getStringArray(R.array.Ekaterinburg);
        final ArrayAdapter<String> adapterEkaterinburg = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, EkaterinburgArray);
        String [] IgevskArray = getResources().getStringArray(R.array.Igevsk);
        final ArrayAdapter<String> adapterIgevsk = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, IgevskArray);
        String [] KazanArray = getResources().getStringArray(R.array.Kazan);
        final ArrayAdapter<String> adapterKazan = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KazanArray);
        String [] KopeiskArray = getResources().getStringArray(R.array.Kopeisk);
        final ArrayAdapter<String> adapterKopeisk = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KopeiskArray);
        String [] KurganArray = getResources().getStringArray(R.array.Kurgan);
        final ArrayAdapter<String> adapterKurgan = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KurganArray);
        String [] KishtimArray = getResources().getStringArray(R.array.Kishtim);
        final ArrayAdapter<String> adapterKishtim = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, KishtimArray);
        String [] MiassArray = getResources().getStringArray(R.array.Miass);
        final ArrayAdapter<String> adapterMiass = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, MiassArray);
        String [] RostovOnDonArray = getResources().getStringArray(R.array.RostovOnDon);
        final ArrayAdapter<String> adapterRostovOnDon = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, RostovOnDonArray);
        String [] SamaraArray = getResources().getStringArray(R.array.Samara);
        final ArrayAdapter<String> adapterSamara = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, SamaraArray);
        String [] SimArray = getResources().getStringArray(R.array.Sim);
        final ArrayAdapter<String> adapterSim = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, SimArray);
        String [] TimenArray = getResources().getStringArray(R.array.Timen);
        final ArrayAdapter<String> adapterTimen = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, TimenArray);
        String [] UfaArray = getResources().getStringArray(R.array.Ufa);
        final ArrayAdapter<String> adapterUfa = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, UfaArray);
        String [] HabarovskArray = getResources().getStringArray(R.array.Habarovsk);
        final ArrayAdapter<String> adapterHabarovsk = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, HabarovskArray);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.citys_array,android.R.layout.simple_spinner_item);
        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        spinner.setPrompt("Title");
        spinner.setSelection(0);
        final Intent intent = new Intent(uActivityGeoposition.this, uActivityGeopositionNext.class);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position) {
                    case 0: uListViewGeoposition.setAdapter(adapterChelyabinsk); break;
                    case 1: uListViewGeoposition.setAdapter(adapterEkaterinburg); break;

                    case 3: uListViewGeoposition.setAdapter(adapterIgevsk); break;
                    case 4: uListViewGeoposition.setAdapter(adapterKazan); break;
                    case 5: uListViewGeoposition.setAdapter(adapterKopeisk); break;
                    case 6: uListViewGeoposition.setAdapter(adapterKurgan); break;
                    case 7: uListViewGeoposition.setAdapter(adapterKishtim); break;
                    case 8: uListViewGeoposition.setAdapter(adapterMiass); break;

                    case 10: uListViewGeoposition.setAdapter(adapterRostovOnDon); break;
                    case 11: uListViewGeoposition.setAdapter(adapterSamara); break;
                    case 12: uListViewGeoposition.setAdapter(adapterSim); break;
                    case 13: uListViewGeoposition.setAdapter(adapterTimen); break;
                    case 14: uListViewGeoposition.setAdapter(adapterUfa); break;
                    case 15: uListViewGeoposition.setAdapter(adapterHabarovsk); break;

                        default:
                            break;
                }
//                pose = position;
//                intent.putExtra(PUT_POSE, pose);

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        uListViewGeoposition.setAdapter(adapter);
        uListViewGeoposition.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int positionOnList, long id) {
//                poseOnList = positionOnList;
//                intent.putExtra(PUT_POSE_LIST, poseOnList);
                startActivity(intent);
            }
        });
    }
}
